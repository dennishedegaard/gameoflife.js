var running = false;
var in_run = false;
var frames = 0;
var generation = 0;
var dx, dy;

/* Generate an empty board. */
function generate_board(width, height, value) {
    "use strict";
    var board = [];
    for (var y = 0;y < height; y++) {
        board[y] = [];
        for (var x = 0;x < width; x++) {
            board[y][x] = value ? true : false;
        }
    }
    return board;
}

/* go to the next generation of the board. */
function evolve(board, newboard) {
    "use strict";
    var len = board.length;
    for (var y = 0; y < board.length; y++) {
        for (var x = 0; x < board[y].length; x++) {
            var alive = board[y][x];
            var count = 0;
            /* left column. */
            if (y > 0) {
                if (x > 0 && board[y - 1][x - 1] === true) {
                    count++;
                }
                if (board[y - 1][x] === true) {
                    count++;
                }
                if (x < len - 1 && board[y - 1][x + 1] === true) {
                    count++;
                }
            }
            /* middle column. */
            if (x > 0 && board[y][x - 1] === true) {
                count++;
            }
            if (x < len - 1 && board[y][x + 1] === true){
                count++;
            }
            /* right column. */
            if (y < len - 1) {
                if (x > 0 && board[y + 1][x - 1] === true) {
                    count++;
                }
                if (board[y + 1][x] === true) {
                    count++;
                }
                if (x < len - 1 && board[y + 1][x + 1] === true) {
                    count++;
                }
            }

            /* populate the new board. */
            if (alive === true && count < 2 || count > 3) {
                newboard[y][x] = false;
            }
            else if (alive === true && count === 2 || count === 3) {
                newboard[y][x] = true;
            }
            else if (alive === false && count === 3) {
                newboard[y][x] = true;
            }
            else {
                newboard[y][x]= alive;
            }
        }
    }
}

function fpsCounter() {
    "use strict";
    $("#fps").text(frames);
    frames = 0;
    if (running) {
        setTimeout(fpsCounter, 1000);
    }
}

$(function() {
    "use strict";
    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext('2d');
    ctx.lineWidth = 1;

    /* offscreen canvas, for prerendering. */
    var offscreen = document.createElement('canvas');
    offscreen.width = canvas.width;
    offscreen.height = canvas.height;
    var offCtx = offscreen.getContext('2d');

    var gridwidth = 100;
    var gridcolor = '#001100';
    var bgcolor = '#000000';
    var fgcolor = '#00FF00';
    var width = $("#canvas").attr('width');
    var height = $("#canvas").attr('height');

    /* boards to switch between. */
    var board1 = generate_board(gridwidth, gridwidth, false);
    var board2 = generate_board(gridwidth, gridwidth, false);
    var firstboard = true;

    $.resetBoard = function(pattern) {
        board1 = generate_board(gridwidth, gridwidth, false);
        board2 = generate_board(gridwidth, gridwidth, false);
        renderboard = generate_board(gridwidth, gridwidth, true);
        firstboard = true;
        // make an acorn in the board, for testing.

        var half = parseInt(gridwidth / 2, 10);
        // if a pattern was given, put the pattern into an empty board.
        if (pattern === 'acorn') {
            /*
              .x.....
              ...x...
              xx..xxx
            */            
            board1[half + 0][half + 1] = true;
            board1[half + 1][half + 3] = true;
            board1[half + 2][half + 0] = true;
            board1[half + 2][half + 1] = true;
            board1[half + 2][half + 4] = true;
            board1[half + 2][half + 5] = true;
            board1[half + 2][half + 6] = true;
        }
        else if (pattern === 'glider') {
            /*
              .x.
              ..x
              xxx
             */
            board1[half + 0][half + 1] = true;
            board1[half + 1][half + 2] = true;
            board1[half + 2][half + 0] = true;
            board1[half + 2][half + 1] = true;
            board1[half + 2][half + 2] = true;
        }
        else if (pattern === 'glidergun') {
            board1[5][1] = true;
            board1[5][2] = true;
            board1[6][1] = true;
            board1[6][2] = true;

            board1[3][13] = true;
            board1[3][14] = true;
            board1[4][12] = true;
            board1[4][16] = true;
            board1[5][11] = true;
            board1[5][17] = true;
            board1[6][11] = true;
            board1[6][15] = true;
            board1[6][17] = true;
            board1[6][18] = true;
            board1[7][11] = true;
            board1[7][17] = true;
            board1[8][12] = true;
            board1[8][16] = true;
            board1[9][13] = true;
            board1[9][14] = true;

            board1[1][25] = true;
            board1[2][23] = true;
            board1[2][25] = true;
            board1[3][21] = true;
            board1[3][22] = true;
            board1[4][21] = true;
            board1[4][22] = true;
            board1[5][21] = true;
            board1[5][22] = true;
            board1[6][23] = true;
            board1[6][25] = true;
            board1[7][25] = true;

            board1[3][35] = true;
            board1[3][36] = true;
            board1[4][35] = true;
            board1[4][36] = true;
        }
        else if (pattern ==='diehard') {
            /*
              ......x.
              xx......
              .x...xxx
             */
            board1[half + 0][half + 6] = true;
            board1[half + 1][half + 0] = true;
            board1[half + 1][half + 1] = true;
            board1[half + 2][half + 1] = true;
            board1[half + 2][half + 5] = true;
            board1[half + 2][half + 6] = true;
            board1[half + 2][half + 7] = true;
        }
        else if (pattern ==='rpentomino') {
            /*
              .xx
              xx.
              .x.
             */
            board1[half + 0][half + 1] = true;
            board1[half + 0][half + 2] = true;
            board1[half + 1][half + 0] = true;
            board1[half + 1][half + 1] = true;
            board1[half + 2][half + 1] = true;
        }
        else if (pattern === 'random') {
            /* generates randomly alive/dead cells. */
            for (var y = 0;y < gridwidth;y++) {
                for (var x = 0; x < gridwidth;x++) {
                    board1[y][x] = (Math.random() > 0.50);
                }
            }
        }
    };

    $.getBoard = function() {
        return firstboard ? board1 : board2;
    };

    /* board to tell if a re-render is needed. */
    var renderboard = generate_board(gridwidth, gridwidth, true);

    dx = parseInt(width / gridwidth, 10);
    dy = parseInt(height / gridwidth, 10);

    $.resetBoard('acorn');

    // initial rendering (background and gridlines).
    // fill board
    offCtx.fillStyle = bgcolor;
    offCtx.fillRect(0, 0, width, height);

    // paint grid
    offCtx.strokeStyle = gridcolor;
    offCtx.beginPath();
    for (var i = 0;i < gridwidth; i++) {
        offCtx.moveTo(dx * i, 0);
        offCtx.lineTo(dx * i, height);
        offCtx.moveTo(0, dy * i);
        offCtx.lineTo(width, dy * i);
    }
    offCtx.stroke();
    offCtx.closePath();

    $.paintBoard = function() {
        // paint fields
        offCtx.fillStyle = fgcolor;
        offCtx.moveTo(0, 0);

        var board = firstboard ? board1 : board2;

        var len = board.length;
        for (var y = 0; y < len; y++) {
            for (var x = 0; x < len; x++) {
                if (renderboard[y][x] === true) {
                    offCtx.fillStyle = (board[y][x] ? fgcolor : bgcolor);
                    offCtx.fillRect(x * dx, y * dy, dx - 1, dy - 1);
                }
            }
        }
        ctx.drawImage(offscreen, 0, 0);

        $("#generation").text(generation);
    };

    $.next = function() {
        var board = firstboard ? board1 : board2;
        var newboard = firstboard ? board2 : board1;
        evolve(board, newboard);
        generation++;

        /* check what content to redraw. */
        var len = renderboard.length;
        for (var y = 0;y < len;y++) {
            for (var x = 0; x < len;x++) {
                if (newboard !== board) {
                    renderboard[y][x] = true;
                }
                else {
                    renderboard[y][x] = false;
                }
            }
        }

        $.paintBoard();
        frames++;
        $("#generation").text(generation);
        firstboard = !firstboard;
    };

    $.run = function() {
        if (!running) {
            return;
        }
        in_run = true;

        $.next();

        in_run = false;
        if (running) {
            setTimeout($.run, 0);
        }
        else {
            $("#btn-start").removeClass('disabled');
        }
    };

    $.paintBoard();
});

/* eventhandlers for buttons. */
$(function() {
    "use strict";
    var btn_start = $("#btn-start");
    var btn_next = $("#btn-next");
    var btn_pause = $("#btn-pause");
    var btn_reset = $("#btn-reset");
    var btn_random = $("#btn-random");
    var btn_load = $("#btn-load");
    var sel_load = $("#sel-load");

    btn_start.click(function() {
        running = true;
        setTimeout($.run, 0);
        setTimeout(fpsCounter, 1000);
        btn_start.addClass("disabled");
        btn_pause.removeClass("disabled");
        btn_next.addClass('disabled');
    });

    btn_next.click(function() {
        if (running || in_run) {
            return;
        }

        $.next();
    });

    btn_pause.click(function() {
        running = false;
        while (in_run) {
            sleep(0);
        }

        $.paintBoard();

        btn_pause.addClass("disabled");
        btn_next.removeClass('disabled');
        btn_start.removeClass('disabled');
    });

    btn_reset.click(function() {
        running = false;
        while (in_run) {
            sleep(0);
        }

        $.resetBoard('');
        generation = 0;
        $.paintBoard();

        btn_pause.addClass('disabled');
        btn_start.removeClass('disabled');
        btn_next.removeClass('disabled');
    });

    btn_random.click(function() {
        running = false;
        while (in_run) {
            sleep(0);
        }

        $.resetBoard('random');
        generation = 0;
        $.paintBoard();

        btn_pause.addClass('disabled');
        btn_start.removeClass('disabled');
        btn_next.removeClass('disabled');
    });

    btn_load.click(function() {
        var selected = sel_load.find(':selected').text();

        /* Parse the selected patttern. */
        if (selected[0] === '-') {
            return;
        }
        var pattern = '';
        if (selected === 'Glider') {
            pattern = 'glider';
        } else if (selected === 'Glider gun') {
            pattern = 'glidergun';
        } else if (selected === 'Acorn') {
            pattern = 'acorn';
        } else if (selected === 'Diehard') {
            pattern = 'diehard';
        } else if (selected === 'R-pentomino') {
            pattern = 'rpentomino';
        } else {
            alert('Unknown pattern: ' + selected);
            return;
        }

        running = false;
        while (in_run) {
            sleep(0);
        }
        
        $.resetBoard(pattern);
        generation = 0;
        $.paintBoard();

        btn_pause.addClass('disabled');
        btn_start.removeClass('disabled');
        btn_next.removeClass('disabled');
    });
});

$(function() {
    "use strict";
    $("#canvas").click(function(evt) {
        var x = evt.offsetX;
        var y = evt.offsetY;

        running = false;

        while (in_run) {
            sleep(0);
        }

        $("#btn-start").removeClass('disabled');
        $("#btn-next").removeClass('disabled');
        $("#btn-pause").addClass('disabled');

        x = parseInt(x / dx, 10);
        y = parseInt(y / dy, 10);

        var board = $.getBoard();
        board[y][x] = !board[y][x];

        $.paintBoard();
    });
});
